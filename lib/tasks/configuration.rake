namespace :configuration do

  desc "TODO"
  task initial_setup: :environment do
    puts 'DB Drop'
    Rake::Task['db:drop'].invoke
    puts 'DB Create'
    Rake::Task['db:create'].invoke
    puts 'DB Migrate'
    Rake::Task['db:migrate'].invoke
    # puts 'Admin Create'
    # Rake::Task['configuration:create_admin'].invoke
    puts 'END'
  end


  # desc 'create the first admin for this platform'
  # task create_admin: :environment do
  #   admin = Admin.all.first
  #   if admin.nil?
  #     Admin.create(email: 'admin@gmail.com', first_name: 'admin', last_name: 'admin', password: 'password', password_confirmation: 'password')
  #   end
  # end

end